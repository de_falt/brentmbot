from threading import Thread
import time
from time import sleep
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities
from bs4 import BeautifulSoup
import pymorphy2
import telebot
from telebot import types


def finamParser():
	def getNums():
		options = webdriver.ChromeOptions()
		options.add_argument('--ignore-certificate-errors')
		options.add_argument('--ignore-ssl-errors')
		caps = DesiredCapabilities().CHROME
		# caps["pageLoadStrategy"] = "normal"  #  complete
		caps["pageLoadStrategy"] = "eager"  # interactive
		# caps["pageLoadStrategy"] = "none"

		driver = webdriver.Chrome(desired_capabilities=caps, executable_path=r"C:\Code\Python\chromedriver.exe")

		start_time = time.time()

		driver.get(r"https://www.finam.ru/quote/tovary/brent/")

		html = driver.page_source
		driver.close()

		soup = BeautifulSoup(html, "html.parser")

		dif = soup.find(
			"sub", {"class": "PriceInformation__subContainer--2qx"})
		dif = dif.find_all("span")[1]
		dif = dif.text[1:-1]  # изменение с открытия торгов

		extrs = soup.find("div", {"class": "QuoteProfileMinMax__root--2NB"})
		extrs = extrs.find_all("div", {"class": "QuoteProfileMinMax__column--2pT"})
		maxs = []  # день, неделя, месяц, полгода, год
		mins = []
		for i in extrs:
			maxs.append(i.find_all("div")[1].text)
			mins.append(i.find_all("div")[2].text)
		return [dif, maxs, mins]  # открытие торгов, максимумы, минимумы

	global finamData

	while True:
		finamData = getNums()
		sleep(6)


def Gparser():
	global getStuff
	def getStuff(single=True, test=False):
		driver = webdriver.Chrome(r"C:\Code\Python\chromedriver.exe")

		if test == False:
			driver.get("https://www.google.com")
			driver.get('https://www.google.com/search?q=%D0%BD%D0%B5%D1%84%D1%82%D1%8C&newwindow=1&tbm=nws&source=lnt&tbs=sbd:1&sa=X&ved=2ahUKEwiCrp3khuL3AhVBi8MKHS-0DScQpwV6BAgBECE&biw=1920&bih=969&dpr=1')

			html = driver.page_source
			driver.close()

			soup = BeautifulSoup(html, "html.parser")

			bigBlock = soup.find("div", {"id": "rso"})
			blocks = bigBlock.find_all("div", recursive=False)

			if single == True:
				subBlock = blocks[0]
				subBlock = subBlock.find("g-card")
				subBlock = subBlock.find("div")
				subBlock = subBlock.find("div")
				subBlock = subBlock.find("a")
				url = subBlock.get("href")
				subBlock = subBlock.find("div")
				# получаем где раздвоение, нужна вторая фигня
				subBlock = subBlock.find_all("div", recursive=False)
				subBlock = subBlock[1]
				subBlock = subBlock.find_all("div", recursive=False)[1]
				text = subBlock.text
				return [text, url]

			elif single == False:
				result = []
				for i in blocks:
					subBlock = i
					subBlock = subBlock.find("g-card")
					subBlock = subBlock.find("div")
					subBlock = subBlock.find("div")
					subBlock = subBlock.find("a")
					url = subBlock.get("href")
					subBlock = subBlock.find("div")
					# получаем где раздвоение, нужна вторая фигня
					subBlock = subBlock.find_all("div", recursive=False)
					subBlock = subBlock[1]
					subBlock = subBlock.find_all("div", recursive=False)[1]
					text = subBlock.text
					result.append([text, url])
				

		elif test == True:
			soup = BeautifulSoup(open("demo_page.html", encoding="utf8"), "html.parser")
			result = []
			bigBlock = soup.find("div", {"id": "rso"})
			blocks = bigBlock.find_all("div", recursive=False)
			for i in blocks:
				subBlock = i
				subBlock = subBlock.find("g-card")
				subBlock = subBlock.find("div")
				subBlock = subBlock.find("div")
				subBlock = subBlock.find("a")
				url = subBlock.get("href")
				subBlock = subBlock.find("div")
				subBlock = subBlock.find_all("div", recursive=False)
				subBlock = subBlock[1]
				subBlock = subBlock.find_all("div", recursive=False)[1]
				text = subBlock.text
				result.append([text, url])

		return result

	global news, check_num;
	news = {}
	check_num = 0

	last_one = []

	while True:
		new = getStuff()
		if new != last_one:
			news[time.time()] = new
			last_one = new
			check_num += 1

		sleep(30)


def lead():
	global white_list, blacklist, wl_active, bl_active, news, news_num


	global up, down
	up = {}
	up["добыча"] = ["сокращение", "сокращать", "сократить", "сократиться", "уменьшение", "уменьшить", "уменьшать", "уменьшиться", "снижение", "снизить", "снижать", "снизиться", "понизиться", "падение", "падать"]
	up["суэцкий"] = ["заблокирован", "блокировать", "заблокировать", "блокировка", "перекрытие", "перекрытый", "перекрывать", "перекрыть", "застрявший", "застрять", "мель"]
	up["эмбарго"] = None
	up["спрос"] = ["повышенный", "повышение", "повыситься"]
	up["цена"] = ["рост", "повышение"]
	up["локдаун"] = ["китай"]
	up["запасы"] = ["снижение", "уменьшение"]

	down = {}
	up["добыча"] = ["наращивание", "наращивать", "увеличить", "увеличиться", "увеличение", "повысить", "повыситься", "повышение", "рост"]
	# up["суэцкий"] = ["застрявший", "застрять", "заблокирован", "заблокировать", "блокировка", "блокировать" "перекрытие", "перекрытый", "перекрывать", "мель"]
	up["спрос"] = ["пониженный", "понижение", "понизиться", "падать", "падение"]
	up["цена"] = ["падение", "упасть", "снижение", "снизится"]

	global checkArticle
	def checkArticle(article):
		global up, down
		morph = pymorphy2.MorphAnalyzer()
		article = article.split()

		for i in range(len(article)):
			article[i] = morph.parse(article[i])[0].normal_form

		for i in article: # проход по словам заголовка
			for j in list(up.keys()): # проход по ключам словаря
				if i == j:
					for k in article:
						if k in up[j]:
							return 1

		for i in article: # проход по словам заголовка
			for j in list(down.keys()): # проход по ключам словаря
				if i == j:
					for k in article:
						if k in down[j]:
							return -1

		return 0



def BrentM_bot():
	token = ""
	bot = telebot.TeleBot(token)

	global show_monitor, add_bl, add_wl, rm_bl, rm_wl, white_list, blacklist, wl_active, bl_active

	markup = types.ReplyKeyboardMarkup(resize_keyboard=True)
	btn1 = types.KeyboardButton("Демо")
	btn2 = types.KeyboardButton("Котировки Brent")
	btn3 = types.KeyboardButton("Получить прогноз")
	btn4 = types.KeyboardButton("Открытый мониторинг")
	btn5 = types.KeyboardButton("Белый список")
	btn6 = types.KeyboardButton("Черный список")
	markup.add(btn1, btn2, btn3, btn4, btn5, btn6)

	prediction_markup = types.ReplyKeyboardMarkup(resize_keyboard=True)
	btn1 = types.KeyboardButton("2 часа")
	btn2 = types.KeyboardButton("24 часа")
	btn3 = types.KeyboardButton("72 часа")
	btn4 = types.KeyboardButton("Назад")
	prediction_markup.add(btn1, btn2, btn3, btn4)

	w_list_markup = types.ReplyKeyboardMarkup(resize_keyboard=True)
	btn1 = types.KeyboardButton("Вкл/выкл б/с")
	btn2 = types.KeyboardButton("Просмотреть б/с")
	btn3 = types.KeyboardButton("Добавить сайт в б/с")
	btn4 = types.KeyboardButton("Удалить сайт из б/с")
	btn5 = types.KeyboardButton("Назад")
	w_list_markup.add(btn1, btn2, btn3, btn4, btn5)

	b_list_markup = types.ReplyKeyboardMarkup(resize_keyboard=True)
	btn1 = types.KeyboardButton("Вкл/выкл ч/с")
	btn2 = types.KeyboardButton("Просмотреть ч/с")
	btn3 = types.KeyboardButton("Добавить сайт в ч/с")
	btn4 = types.KeyboardButton("Удалить сайт из ч/с")
	btn5 = types.KeyboardButton("Назад")
	b_list_markup.add(btn1, btn2, btn3, btn4, btn5)

	@bot.message_handler(commands=["start"])
	def start(message):
		bot.send_message(message.chat.id, "Данный бот осуществляет настраиваемый анализ новостей, полученных из открытых источников с целью прогноза дальнейшего хода торгов. Помимо этого он предоставляет актуальные котировки и сами прогнозы на основе обработанных данных за выбранный интервал", reply_markup=markup)

	@bot.message_handler(content_types=["text"])
	def handle_text(message):
		global show_monitor, add_bl, add_wl, rm_bl, rm_wl, whitelist, blacklist, wl_active, bl_active, checkArticle

		if message.text == "Демо":
			bot.send_message(message.chat.id, "23 марта 2021 года Суэцкий канал был перекрыт из-за севшего на мель контейнеровоза. Канал является важнейшей торговой артерией, через которую помимо множества товаров перевозится и нефть. Несмотря на то, что канал был заблокирован всего 7 дней и резервные запасы нефти помогли справиться с временным отсутствием новых поставок, событие подверглось особенному интересу со стороны СМИ, в результате чего стоимость нефти выросла на 6 % на следующий же день")
			sleep(8)
			bot.send_message(message.chat.id, "Новости об этом до российских читателей дошли только 24 марта")
			img = open("news_demo.png", 'rb')
			bot.send_photo(message.chat.id, img)
			img.close()
			sleep(4)
			bot.send_message(message.chat.id, "В последующие дни со стоимостью нефти происходило следующее")
			img = open("price_demo.png", 'rb')
			bot.send_photo(message.chat.id, img)
			img.close()
			global getStuff
			array = getStuff(single=False, test=True)
			text = ""
			for i in array:
				text = text + i[0] + "\n\n"
			bot.send_message(message.chat.id, ("После парсинга страницы получаем следующие заголовки:\n\n" + text))
			sleep(8)
			bot.send_message(message.chat.id, ("Проведем их анализ..."))
			k = 0;
			for i in array:
				k += checkArticle(i[0])
				sleep(3)
			bot.send_message(message.chat.id, ("В результате алгоритм разобрал такие ключевые слова, как \"суэцкий\", \"перекрыл\", \"заблокировал\", \"заблокировало\", \"мель\" и \"застрявшего\". На их основе был подсчитан коэффициент k = " + str(k) + ", указавующий на то, что информационная повестка 5 из 10 заголовков теоретически приведет к подорожания баррели, о чем и свидетельствует график"))

		elif message.text == "Котировки Brent":
			global finamData # открытие торгов, максимумы, минимумы
			text = "Изменение с открытия торгов: " + finamData[0] + "\n\nМаксимум за день: " + finamData[1][1] + "\nМинимум за день: " + finamData[2][1] 
			text += ("\n\nМаксимум за неделю: " + finamData[1][2] + "\nМинимум за неделю: " + finamData[2][2])
			text += ("\n\nМаксимум за месяц: " + finamData[1][3] + "\nМинимум за месяц: " + finamData[2][3])
			text += ("\n\nМаксимум за полгода: " + finamData[1][4] + "\nМинимум за полгода: " + finamData[2][4])
			text += ("\n\nМаксимум за год: " + finamData[1][5] + "\nМинимум за год: " + finamData[2][5])
			bot.send_message(message.chat.id, text)
			bot.send_message(message.chat.id, "Источник: https://www.finam.ru/quote/tovary/brent/")

		elif message.text == "Получить прогноз":
			bot.send_message(message.chat.id, "Выберите времянной промежуток для получения прогноза", reply_markup=prediction_markup)

		elif message.text == "Открытый мониторинг":
			if show_monitor == False:
				show_monitor = True
				bot.send_message(message.chat.id, "Включен режим активного мониторинга. Теперь вам будут приходить все новые статьи по теме")
			else:
				show_monitor = False
				bot.send_message(message.chat.id, "Режим активного мониторинга выключен. Вам не будут приходить новые статьи, но их обработка продолжится")

		elif message.text == "Белый список":
			bot.send_message(message.chat.id, "Настройки белого списка", reply_markup=w_list_markup)

		elif message.text == "Черный список":
			bot.send_message(message.chat.id, "Настройки черного списка", reply_markup=b_list_markup)

		elif message.text == "Вкл/выкл б/с":
			if wl_active == False:
				wl_active = True
				bot.send_message(message.chat.id, "Белый список активирован")
			else:
				wl_active = False
				bot.send_message(message.chat.id, "Белый список деактивирован")

		elif message.text == "Просмотреть б/с":
			if len(whitelist) == 0:
				bot.send_message(message.chat.id, "Белый список пуст")

			else:
				text = "Сайты в белом списке:\n"
				for i in whitelist:
					text = text + i + "\n\n"

				bot.send_message(message.chat.id, text)

		elif message.text == "Добавить сайт в б/с":
			add_wl = True
			bot.send_message(message.chat.id, "Пришлите ссылку на сайт для добавление в белый список")

		elif message.text == "Удалить сайт из б/с":
			rm_wl = True
			bot.send_message(message.chat.id, "Пришлите ссылку на сайт для удаления из белого списка")

		elif message.text == "Вкл/выкл ч/с":
			if bl_active == False:
				bl_active = True
				bot.send_message(message.chat.id, "Черный список активирован")
			else:
				bl_active = False
				bot.send_message(message.chat.id, "Черный список деактивирован")

		elif message.text == "Просмотреть ч/с":
			if len(blacklist) == 0:
				bot.send_message(message.chat.id, "Черный список пуст")

			else:
				text = "Сайты в черном списке:\n"
				for i in blacklist:
					text = text + i + "\n\n"

				bot.send_message(message.chat.id, text)

		elif message.text == "Добавить сайт в ч/с":
			add_bl = True
			bot.send_message(message.chat.id, "Пришлите ссылку на сайт для добавление в черный список")

		elif message.text == "Удалить сайт из ч/с":
			rm_bl = True
			bot.send_message(message.chat.id, "Пришлите ссылку на сайт для добавление в белый список")

		elif add_wl == True:
			add_wl = False
			if message.text in whitelist:
				bot.send_message(message.chat.id, ("В белый список уже занесено введенное значение:\n" + message.text))

			else:
				whitelist.append(message.text)
				bot.send_message(message.chat.id, ("В белый список добавлено следующее значение:\n" + message.text))


		elif rm_wl == True:
			rm_wl = False
			if message.text in whitelist:
				whitelist.remove(message.text)
				bot.send_message(message.chat.id, "Сайт удален из белого списка")
			else:
				bot.send_message(message.chat.id, "В белом списке нет такого сайта")



		elif add_bl == True:
			add_bl = False
			if message.text in blacklist:
				bot.send_message(message.chat.id, ("В белый список уже занесено введенное значение:\n" + message.text))

			else:
				blacklist.append(message.text)
				bot.send_message(message.chat.id, ("В белый список добавлено следующее значение:\n" + message.text))


		elif rm_bl == True:
			rm_bl = False
			if message.text in blacklist:
				blacklist.remove(message.text)
				bot.send_message(message.chat.id, "Сайт удален из черного списка")
			else:
				bot.send_message(message.chat.id, "В черном списке нет такого сайта")





		elif message.text == "Назад":
			bot.send_message(message.chat.id, "Переход в исходное меню", reply_markup=markup)

	bot.polling(none_stop=True)


whitelist = []
wl_active = False
blacklist = []
bl_active = False
show_monitor = False
add_bl = False
add_wl = False
rm_bl = False
rm_wl = False
news = {}
check_num = 0

th1 = Thread(target=finamParser)
th2 = Thread(target=Gparser)
th3 = Thread(target=lead)
th4 = Thread(target=BrentM_bot)


th1.start()
th2.start()
th3.start()
th4.start()
